# gpio-leds gateware

To install this gateware

``` bash
wget https://beaglev-fire.beagleboard.io/BeagleV-Fire-ubuntu/get_kernel_update.sh

chmod +x ./get_kernel_update.sh

sudo ./get_kernel_update.sh

sudo apt-get update

sudo apt-get install -y software-properties-common

sudo add-apt-repository "deb [trusted=yes] http://lorforlinux.beagleboard.io/gpio-leds-gateware stable main"

sudo apt-get install -y bbb.io-gateware-my-custom-fpga-design

sudo /usr/share/beagleboard/gateware/change-gateware.sh /usr/share/beagleboard/gateware/my_custom_fpga_design/
```

After reboot you'll be able to control the gpio pins from `/sys/class/leds` interface.

# BeagleV Fire Gateware Builder

## Introduction
The BeagleV Fire gateware builder is a Python script that builds both the PolarFire SoC HSS bootloader and Libero FPGA project into a single programming bitstream. It uses a list of repositories/branches specifying the configuration of the BeagleV Fire to build.


## Prerequisites
### Python libraries
The following Python libraries are used:
- GitPython
- PyYAML

```
pip3 install gitpython
pip3 install pyyaml
```

### Microchip Tools
The SoftConsole and Libero tools from Microchip are required by the bitstream builder.

The following environment variables are required for the bitstream builder to use the Microchip tools:
- SC_INSTALL_DIR
- FPGENPROG
- LIBERO_INSTALL_DIR
- LM_LICENSE_FILE

An example script for setting up the environment is available [here](https://git.beagleboard.org/beaglev-fire/Microchip-FPGA-Tools-Setup). 

## Usage

```
python3 build-bitstream.py <YAML Configuration File>
```

For example, the following command will be build the default beagleV Fire configuration:
```
python3 build-bitstream.py ./build-options/default.yaml
```


### YAML Configuration Files
The YAML configuration files are located in the "build-options" directory.

| Configuration File | Description                                                |
| ------------------ | ---------------------------------------------------------- |
| default.yaml       | Default gateware including default cape and M.2 interface. |
| minimal.yaml       | Minimal Linux system including Ethernet. No FPGA gateware. |
| robotics.yaml      | Similar to default but supporting the Robotics cape.       |

## Supported Platforms
The BeagleV Fire gateware builder has been tested on Ubuntu 20.04.

## Microchip bitstream-builder
The BeagleV-Fire gateware builder is derived from [Microchip's bitstream-builder ](https://github.com/polarfire-soc/icicle-kit-minimal-bring-up-design-bitstream-builder). We recommend that you use either of these scripts as a starting point for your own PolarFire SoC FPGA designs as opposed to using Libero in isolation.
